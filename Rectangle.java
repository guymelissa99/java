public class Rectangle{

    int longueur;
    int largeur;

    public Rectangle(int longueur, int largeur) {
        this.longueur = longueur;
        this.largeur = largeur;
    }

    public void display() {

        System.out.println("###");
        System.out.println("###");
        System.out.println("###");
        
    }

    public static void main(String[] args){
    
        new Rectangle(3,5).display();
    }
}
