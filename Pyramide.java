public class Pyramide{
    public static void main(String[] args){

        String maChaine = "bonjour!";
        int size = maChaine.length();
        
        for(int i=1;i<=size;i++) {

            for(int j=0; j<size-i; j++){
                System.out.print(" ");
            }
            
            for(int k = 0; k <= i ; k++){
                System.out.print(maChaine.substring(0, k));
            }

            System.out.println();

        }
    }
}
